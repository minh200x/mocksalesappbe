const express = require("express");
const app = express();
const port = 1002;
app.listen(port, () => {
  console.log(`Mock API is running at http://localhost:${port}`);
});

// ------------------------------------------------------------------------------------------------

app.post("/saleskit/tools/list", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        id: 7,
        toolName: "Tính phí L/C nhập khẩu",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: null,
        status: 1,
      },
      {
        id: 11,
        toolName: "Lãi suất huy động",
        customerId: "KHCN",
        customerType: "KHCN",
        priority: null,
        status: 1,
      },
      {
        id: 12,
        toolName: "Phí chuyển tiền quốc tế",
        customerId: "KHCN",
        customerType: "KHCN",
        priority: null,
        status: 1,
      },
      {
        id: 10,
        toolName: "Tính phí bảo lãnh",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: null,
        status: 1,
      },
      {
        id: 6,
        toolName: "Lịch trả nợ dự kiến",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: "QUANTRONG",
        status: 1,
      },
      {
        id: 3,
        toolName: "Thẻ tín dụng KHDN",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: "BINHTHUONG",
        status: 1,
      },
      {
        id: 8,
        toolName: "Sản phẩm huy động vốn",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: null,
        status: 1,
      },
      {
        id: 2,
        toolName: "Phương thức vay vốn",
        customerId: "KHCN",
        customerType: "KHCN",
        priority: "RATQUANTRONG",
        status: 1,
      },
      {
        id: 1,
        toolName: "Thẻ tín dụng KHCN",
        customerId: "KHCN",
        customerType: "KHCN",
        priority: "QUANTRONG",
        status: 1,
      },
      {
        id: 4,
        toolName: "Tra cứu tỷ lệ ký quỹ bảo lãnh",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: "QUANTRONG",
        status: 1,
      },
      {
        id: 9,
        toolName: "Tính phí chuyển tiền đi nước ngoài",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: null,
        status: 1,
      },
      {
        id: 5,
        toolName: "Tra cứu lãi suất",
        customerId: "KHDN",
        customerType: "KHDN",
        priority: "QUANTRONG",
        status: 1,
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.get("/saleskit/CB/loanInterestRates/getInfo", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        listCurrency: [
          {
            id: 30,
            type: "CURRENCY",
            value: "VND",
            name: "VND",
          },
          {
            id: 31,
            type: "CURRENCY",
            value: "USD",
            name: "USD",
          },
        ],
        listCreditRate: [
          {
            id: 32,
            type: "CREDIT_RATE",
            value: "AAA",
            name: "AAA",
          },
          {
            id: 33,
            type: "CREDIT_RATE",
            value: "AA",
            name: "AA",
          },
          {
            id: 34,
            type: "CREDIT_RATE",
            value: "A",
            name: "A",
          },
          {
            id: 35,
            type: "CREDIT_RATE",
            value: "BBB",
            name: "BBB",
          },
          {
            id: 36,
            type: "CREDIT_RATE",
            value: "BB",
            name: "BB",
          },
          {
            id: 37,
            type: "CREDIT_RATE",
            value: "B",
            name: "B",
          },
          {
            id: 38,
            type: "CREDIT_RATE",
            value: "CCC",
            name: "CCC",
          },
          {
            id: 39,
            type: "CREDIT_RATE",
            value: "CC",
            name: "CC",
          },
          {
            id: 40,
            type: "CREDIT_RATE",
            value: "C",
            name: "C",
          },
        ],
        listFinancialReport: [
          {
            id: 41,
            type: "FINANCIAL_REPORT",
            value: "Khách hàng có nộp BCTC kiểm toán/ thuế",
            name: "BCTC_KIEMTOAN_THUE",
          },
          {
            id: 42,
            type: "FINANCIAL_REPORT",
            value: "Khách hàng nộp BCTC nội bộ",
            name: "BCTC_NOIBO",
          },
          {
            id: 43,
            type: "FINANCIAL_REPORT",
            value: "Khách hàng không nộp BCTC kiểm toán/ thuế",
            name: "BCTC_KHONGNOP_BCTC",
          },
        ],
        listLoansProductSpecial: [
          {
            id: 44,
            type: "LOANS_PRODUCTS_SPECIAL",
            value: "Không có sản phẩm vay đặc biệt",
            name: "SPDB_KHONGCO",
          },
          {
            id: 45,
            type: "LOANS_PRODUCTS_SPECIAL",
            value: "KH đầu tư dự án mới cùng ngành/mở rộng",
            name: "SPDB_DAUTU_CUNGNGANH",
          },
          {
            id: 46,
            type: "LOANS_PRODUCTS_SPECIAL",
            value:
              "KH đầu tư dự án mới khác ngành/ hoặc KH được phân luồng tín dụng đỏ",
            name: "SPDB_DAUTU_KHACNGANH",
          },
        ],
        listCollateralDto: [
          {
            id: "TSDB1",
            name: "STK/GTCG/HĐTG VND do TPbank phát hành",
          },
          {
            id: "TSDB2",
            name: "CCTG do TPbank phát hành",
          },
          {
            id: "TSDB3",
            name: "Trái phiếu do TPbank phát hành",
          },
          {
            id: "TSDB4",
            name: "Trái phiếu doanh nghiệp",
          },
          {
            id: "TSDB5",
            name: "STK/GTCG/HĐTG VND hoặc USD do TCTD khác phát hành hoặc GTCG do CP VND phát hành/ Vàng cầm cố tại TPbank",
          },
          {
            id: "TSDB6",
            name: "Bất động sản, oto",
          },
          {
            id: "TSDB7",
            name: "Máy móc, thiết bị, phương tiện vận tải khác",
          },
          {
            id: "TSDB8",
            name: "Tài sản đảm bảo khác",
          },
          {
            id: "TSDB9",
            name: "Không có tài sản đảm bảo",
          },
          {
            id: "TSDB10",
            name: "STK/GTCG/HĐTG USD do TPbank phát hành",
          },
        ],
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.post("/saleskit/CB/loanInterestRates/caculator", (req, res) => {
  const response = {
    code: "500",
    message: null,
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.get("/saleskit/guaranteemarginrate/input", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        listCreditRate: [
          {
            id: 32,
            type: "CREDIT_RATE",
            value: "AAA",
            name: "AAA",
          },
          {
            id: 33,
            type: "CREDIT_RATE",
            value: "AA",
            name: "AA",
          },
          {
            id: 34,
            type: "CREDIT_RATE",
            value: "A",
            name: "A",
          },
          {
            id: 35,
            type: "CREDIT_RATE",
            value: "BBB",
            name: "BBB",
          },
          {
            id: 36,
            type: "CREDIT_RATE",
            value: "BB",
            name: "BB",
          },
          {
            id: 37,
            type: "CREDIT_RATE",
            value: "B",
            name: "B",
          },
          {
            id: 38,
            type: "CREDIT_RATE",
            value: "CCC",
            name: "CCC",
          },
          {
            id: 39,
            type: "CREDIT_RATE",
            value: "CC",
            name: "CC",
          },
        ],
        listNganhNghe: [
          {
            id: 47,
            type: "NGANH_NGHE",
            value: "Xây lắp",
            name: "XL",
          },
          {
            id: 48,
            type: "NGANH_NGHE",
            value: "Dược phẩm và trang thiết bị y tế",
            name: "DP_TTBYT",
          },
          {
            id: 49,
            type: "NGANH_NGHE",
            value: "Nhà thầu EVN",
            name: "EVN",
          },
          {
            id: 50,
            type: "NGANH_NGHE",
            value: "Ngành nghề khác",
            name: "NNK",
          },
        ],
        listGuaranteeProduct: [
          {
            id: 1,
            name: "Bảo lãnh dự thầu",
          },
          {
            id: 2,
            name: "Bảo lãnh thanh toán",
          },
          {
            id: 3,
            name: "Bảo lãnh vay vốn",
          },
          {
            id: 4,
            name: "Bảo lãnh thanh toán tiền nộp thuế",
          },
          {
            id: 5,
            name: "Bảo lãnh dự thầu (đấu thầu bất động sản)",
          },
          {
            id: 6,
            name: "Bảo lãnh bảo hành",
          },
          {
            id: 7,
            name: "Bảo lãnh tạm ứng",
          },
          {
            id: 8,
            name: "Bảo lãnh thực hiện hơp đồng",
          },
          {
            id: 9,
            name: "Bảo lãnh hoàn quyết toán",
          },
          {
            id: 10,
            name: "Bảo lãnh đảm bảo môi trường, xã hội và sức khỏe",
          },
        ],
        getAllByStatus: [
          {
            id: 1,
            name: "BigCorp1 ",
          },
          {
            id: 2,
            name: "Upper SMEs2 ",
          },
          {
            id: 3,
            name: "Upper SMEs1",
          },
          {
            id: 4,
            name: "Medium Enterprise 2",
          },
          {
            id: 5,
            name: "Medium Enterprise 1 ",
          },
          {
            id: 6,
            name: "Small Enterprise",
          },
          {
            id: 7,
            name: "Micro Enterprise",
          },
        ],
        marginRate: null,
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.post("/saleskit/guaranteemarginrate/output", (req, res) => {
  const response = {
    message: {
      code: "201",
      message: "No record found",
    },
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.get("/saleskit/creditcardcb/input", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        allObj: [
          {
            id: 1,
            name: "TOP UP CARD",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 2,
            name: "ADD ON CARD",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
        ],
        allPt: [
          {
            id: 1,
            name: "Cấp tín dụng từng lần",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 2,
            name: "Cấp tín dụng hạn mức",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
        ],
        allHt: [
          {
            id: 1,
            name: "Cho vay",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 2,
            name: "BL/LC",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 3,
            name: "Hạn mức cho vay",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 4,
            name: "Hạn mức BL/LC",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
        ],
        allHtbl: [
          {
            id: 1,
            name: "Thời gian trong vòng 1 năm ",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 2,
            name: "Thời gian trên 1 năm ",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
        ],
        allHtcv: [
          {
            id: 1,
            name: "Thời gian theo từng lần trong vòng 1 năm",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 2,
            name: "Thời gian theo từng lần trên 1 năm",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
          {
            id: 3,
            name: "Vay trung, dài hạn và trải qua thời gian trả nợ tối thiểu 40% giá trị khoản vay",
            status: 0,
            createdBy: null,
            createDate: null,
            updateBy: null,
            updateDate: null,
          },
        ],
        creditLimit: null,
        cusSegment: null,
        creditValueUp: null,
        creditValueDown: null,
        rank: null,
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.post("/saleskit/creditcardcb/output", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        allObj: null,
        allPt: null,
        allHt: null,
        allHtbl: null,
        allHtcv: null,
        creditLimit: 100000000.0,
        cusSegment: "Micro Enterprise",
        creditValueUp: 100000000,
        creditValueDown: 20000000,
        rank: "Thẻ bạch kim",
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.get("/saleskit/RB/creditCard/getInfo", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        listPolicyCard: [
          {
            id: 54,
            type: "POLICY_CARD_RB",
            value: "KH là Hội viên KNDL của MobiFone",
            name: "MEMBER_OF_MOBIFONE",
          },
          {
            id: 55,
            type: "POLICY_CARD_RB",
            value: "KH là Hội viên Bông sen vàng của VNA",
            name: "MEMBER_OF_VNA",
          },
          {
            id: 56,
            type: "POLICY_CARD_RB",
            value: "KH đang sở hữu Thẻ tín dụng của Ngân hàng khác",
            name: "CARD_WITH_OTHER_BANK",
          },
          {
            id: 51,
            type: "POLICY_CARD_RB",
            value: "KH trả lương",
            name: "PAY_FORMS",
          },
          {
            id: 52,
            type: "POLICY_CARD_RB",
            value: "KH có khoản vay tại TPBank",
            name: "LOANS_WITH_TPBANK",
          },
          {
            id: 53,
            type: "POLICY_CARD_RB",
            value: "KH đang có sổ tiết kiệm tại TPBank",
            name: "SAVING_WITH_TPBANK",
          },
        ],
        listPayForms: [
          {
            id: 57,
            type: "PAY_FORMS",
            value: "Trả lương tại TPBank",
            name: "PAY_FORMS_TPBANK",
          },
          {
            id: 58,
            type: "PAY_FORMS",
            value: "Trả lương tại Ngân hàng khác",
            name: "PAY_FORMS_OTHER_BANK",
          },
          {
            id: 59,
            type: "PAY_FORMS",
            value: "Trả lương tiền mặt",
            name: "PAY_FORMS_CASH",
          },
        ],
        listRankMemberMobifone: [
          {
            id: 60,
            type: "RANK_MEMBER_OF_MOBIFONE",
            value: "Titan",
            name: "TITAN_MEMBER_MOBIFONE",
          },
          {
            id: 61,
            type: "RANK_MEMBER_OF_MOBIFONE",
            value: "Vàng",
            name: "GOLD_MEMBER_MOBIFONE",
          },
          {
            id: 62,
            type: "RANK_MEMBER_OF_MOBIFONE",
            value: "Kim cương",
            name: "DIAMON_MEMBER_MOBIFONE",
          },
        ],
        listTypeMemberMobifone: [
          {
            id: 63,
            type: "TYPE_MEMBER_OF_MOBIFONE",
            value: "Thuê bao trả trước",
            name: "PRE_PAID_ACCOUNT",
          },
          {
            id: 64,
            type: "TYPE_MEMBER_OF_MOBIFONE",
            value: "Thuê bao trả sau",
            name: "AFTER_PAID_ACCOUNT",
          },
        ],
        listBank: [
          "OTHER",
          "VIB",
          "Nam A Bank",
          "ACB",
          "VietinBank",
          "SCB",
          "SeABank",
          "MBBank",
          "Shinhan Bank",
          "VPBank",
          "Standard Chartered",
          "Vietcombank",
          "Sacombank",
          "Techcombank",
          "BIDV",
          "HSBC",
          "Citibank",
          "Eximbank",
        ],
        listRankCardOtherBank: [
          {
            id: 68,
            type: "RANK_CARD_OTHER_BANK",
            value: "Chuẩn",
            name: "STANDARD",
          },
          {
            id: 69,
            type: "RANK_CARD_OTHER_BANK",
            value: "Vàng",
            name: "GOLD",
          },
          {
            id: 70,
            type: "RANK_CARD_OTHER_BANK",
            value: "Platinum",
            name: "PLATINUM",
          },
        ],
        listUseRate: [
          {
            id: 30,
            type: "USE_RATE_CARD_RB",
            from: "0",
            to: "30",
            equal: null,
            toolsId: null,
            description: null,
            status: 1,
            createDate: 1609866000000,
            createBy: "DUNGTQ",
            modifyBy: null,
            modifyDate: null,
          },
          {
            id: 31,
            type: "USE_RATE_CARD_RB",
            from: "30",
            to: "50",
            equal: null,
            toolsId: null,
            description: null,
            status: 1,
            createDate: 1609866000000,
            createBy: "DUNGTQ",
            modifyBy: null,
            modifyDate: null,
          },
          {
            id: 32,
            type: "USE_RATE_CARD_RB",
            from: "50",
            to: "70",
            equal: null,
            toolsId: null,
            description: null,
            status: 1,
            createDate: 1609866000000,
            createBy: "DUNGTQ",
            modifyBy: null,
            modifyDate: null,
          },
          {
            id: 34,
            type: "USE_RATE_CARD_RB",
            from: "70",
            to: null,
            equal: null,
            toolsId: null,
            description: null,
            status: 1,
            createDate: 1609866000000,
            createBy: "DUNGTQ",
            modifyBy: null,
            modifyDate: null,
          },
        ],
        listRankMemberVNA: [
          {
            id: 65,
            type: "RANK_MEMBER_OF_VNA",
            value: "Titan",
            name: "TITAN_MEMBER_VNA",
          },
          {
            id: 66,
            type: "RANK_MEMBER_OF_VNA",
            value: "Vàng",
            name: "GOLD_MEMBER_VNA",
          },
          {
            id: 67,
            type: "RANK_MEMBER_OF_VNA",
            value: "Platinum",
            name: "PLATINUM_MEMBER_VNA",
          },
        ],
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.post("saleskit/RB/creditCard/calculator", (req, res) => {
  const response = {
    message: {
      code: "200",
      message: null,
    },
    data: [
      {
        check: null,
        reason: null,
        cardType: "Thẻ tín dụng quốc tế TPBank Visa",
        listCard: null,
      },
    ],
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------

app.post("/saleskit/news/insert", (req, res) => {
  const response = {
    message: {
      code: "400",
      message: "400",
    },
  };
  setTimeout(() => {
    res.json(response);
  }, 300);
});

// ------------------------------------------------------------------------------------------------
